$(document).ready(function() {


  //Hamburger Menu Open
  $("#open").click(function() {
    $("nav").css("width", "100%");
    $("#close").toggleClass("hide");
  });

  //Hamburger Menu Close
  $("#close").click(function() {
    $("nav").css("width", "0%");
    $("#close").toggleClass("hide");
  });

  //Hamburger Menu Close if click
  $("nav a").click(function() {
    $("nav").css("width", "0%");
    $("#close").toggleClass("hide");
  });

  //Mobile View Navigation
  $(".top").click(function() {
    window.open("#top", "_self");
  });
  
  //Contact button for header
  $(".headbutt").click(function() {
    window.open("contact.html", "_self");
  });

  //Scroll to top buton (link to take it back to the top of the page)
  $(".top").click(function() {
    window.open("#top", "_self");
  });
});


// scroll to top button
$(document).scroll(function() {
  //calculates the scroll length
  var scroll = $(this).scrollTop();
  if (scroll > 100) {
    //once scrolled 100px show button
    $(".top").fadeIn();
  } else {
    //if not scrolled don't show button
    $(".top").fadeOut();
  }
});
